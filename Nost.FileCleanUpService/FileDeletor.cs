﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Nost.FileCleanUpService
{
    public partial class FileDeletor : ServiceBase
    {
        Timer timer;

        public FileDeletor()
        {
            InitializeComponent();
            AutoLog = false;
            ServiceName = nameof(FileDeletor) + " Service";
            string logName = nameof(FileDeletor) + " Logging";

            ((ISupportInitialize)EventLog).BeginInit();
            if (!EventLog.SourceExists(ServiceName))
            {
                EventLog.CreateEventSource(ServiceName, logName);
            }
            ((ISupportInitialize)EventLog).EndInit();

            EventLog.Source = ServiceName;
            EventLog.Log = logName;
        }

        protected override void OnStart(string[] args)
        {
            DeleteFiles();
            EventLog.WriteEntry("In OnStart");

        }

        private void DeleteFiles()
        {
            try
            {
                timer = new Timer();
                timer.Interval = (10000) * 6;
                timer.Elapsed += Timer_Elapsed;
                string[] files = Directory.GetFiles(@"C:\inetpub\wwwroot\PublishOutput", "Pruefung_*_*.pdf");
                foreach (var item in files)
                {
                    if (DateTime.Now.Subtract(File.GetCreationTime(item)).TotalDays > 7)
                    {
                        EventLog.WriteEntry("Delete File: " + item);
                        File.Delete(item);
                    }
                }

                timer.Enabled = true;
                timer.Start();
            }
            catch
            {
            }
        }

        protected override void OnStop()
        {

        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            DeleteFiles();
        }
    }
}
