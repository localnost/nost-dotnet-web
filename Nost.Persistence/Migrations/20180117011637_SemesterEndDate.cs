﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Nost.Persistence.Migrations
{
    public partial class SemesterEndDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SemesterEndId",
                table: "Exams",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Exams_SemesterEndId",
                table: "Exams",
                column: "SemesterEndId");

            migrationBuilder.AddForeignKey(
                name: "FK_Exams_SemesterEnd_SemesterEndId",
                table: "Exams",
                column: "SemesterEndId",
                principalTable: "SemesterEnd",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Exams_SemesterEnd_SemesterEndId",
                table: "Exams");

            migrationBuilder.DropIndex(
                name: "IX_Exams_SemesterEndId",
                table: "Exams");

            migrationBuilder.DropColumn(
                name: "SemesterEndId",
                table: "Exams");
        }
    }
}
