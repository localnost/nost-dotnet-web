﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Nost.Persistence.Migrations
{
    public partial class SchoolClasses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Student_Teacher_ClassTeacherId",
                table: "Student");

            migrationBuilder.DropIndex(
                name: "IX_Student_ClassTeacherId",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "ClassTeacherId",
                table: "Student");

            migrationBuilder.AlterColumn<string>(
                name: "FailedClass",
                table: "Exams",
                type: "nvarchar(7)",
                maxLength: 7,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 5,
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ClassTeacherId",
                table: "Student",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FailedClass",
                table: "Exams",
                maxLength: 5,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(7)",
                oldMaxLength: 7,
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Student_ClassTeacherId",
                table: "Student",
                column: "ClassTeacherId");

            migrationBuilder.AddForeignKey(
                name: "FK_Student_Teacher_ClassTeacherId",
                table: "Student",
                column: "ClassTeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
