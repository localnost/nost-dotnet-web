﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Nost.Persistence.Migrations
{
    public partial class AppointmentAsForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Appointment",
                table: "Exams");

            migrationBuilder.AddColumn<int>(
                name: "AppointmentId",
                table: "Exams",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Exams_AppointmentId",
                table: "Exams",
                column: "AppointmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Exams_ExamAppointment_AppointmentId",
                table: "Exams",
                column: "AppointmentId",
                principalTable: "ExamAppointment",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Exams_ExamAppointment_AppointmentId",
                table: "Exams");

            migrationBuilder.DropIndex(
                name: "IX_Exams_AppointmentId",
                table: "Exams");

            migrationBuilder.DropColumn(
                name: "AppointmentId",
                table: "Exams");

            migrationBuilder.AddColumn<DateTime>(
                name: "Appointment",
                table: "Exams",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
