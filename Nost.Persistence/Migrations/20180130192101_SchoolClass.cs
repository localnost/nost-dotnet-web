﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Nost.Persistence.Migrations
{
    public partial class SchoolClass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Class",
                table: "Student");

            migrationBuilder.AddColumn<string>(
                name: "SchoolClass",
                table: "Student",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SchoolClass",
                table: "Student");

            migrationBuilder.AddColumn<string>(
                name: "Class",
                table: "Student",
                nullable: true);
        }
    }
}
