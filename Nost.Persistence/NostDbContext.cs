﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Nost.Core.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Nost.Persistence
{
    public class NostDbContext : DbContext
    {
        public DbSet<Exam> Exams { get; set; }
        public DbSet<Teacher> Teacher { get; set; }
        public DbSet<Subject> Subject { get; set; }
        public DbSet<Department> Department { get; set; }

        public DbSet<Student> Student { get; set; }

        public DbSet<SchoolClass> SchoolClass { get; set; }

        public DbSet<ExamAppointment> ExamAppointment { get; set; }

        public DbSet<SemesterEnd> SemesterEnd { get; set; }

        public NostDbContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            Console.WriteLine(Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"));
            var builder = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json",
                 optional: true, reloadOnChange: true);
            var configuration = builder.Build();
            string connectionString = configuration["ConnectionStrings:DefaultConnection"];
            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>()
                .HasIndex(s => s.UserName)
                .IsUnique();
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
                relationship.DeleteBehavior = DeleteBehavior.Restrict;

            base.OnModelCreating(modelBuilder);
        }
    }

}

