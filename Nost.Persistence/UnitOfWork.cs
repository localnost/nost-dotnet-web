﻿using Microsoft.EntityFrameworkCore;
using Nost.Core.Contracts;
using Nost.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nost.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed;
        private readonly NostDbContext context = new NostDbContext();

        public IGenericRepository<Exam> ExamRepository { get; }

        public IGenericRepository<Teacher> TeacherRepository { get; }

        public IGenericRepository<Subject> SubjectRepository { get; }

        public IGenericRepository<Department> DepartmentRepository { get; }

        public IGenericRepository<Student> StudentRepository { get; }

        public IGenericRepository<SchoolClass> SchoolClassRepository { get; }

        public IGenericRepository<SemesterEnd> SemesterEndRepository { get; }

        public IGenericRepository<ExamAppointment> ExamAppointmentRepository { get; }

        public UnitOfWork()
        {
            context = new NostDbContext();
            MigrateDatabase();

            ExamRepository = new GenericRepository<Exam>(context);
            TeacherRepository = new GenericRepository<Teacher>(context);
            SubjectRepository = new GenericRepository<Subject>(context);
            DepartmentRepository = new GenericRepository<Department>(context);
            StudentRepository = new GenericRepository<Student>(context);
            SemesterEndRepository = new GenericRepository<SemesterEnd>(context);
            ExamAppointmentRepository = new GenericRepository<ExamAppointment>(context);
        }



        /// <summary>
        ///     Repository-übergreifendes Speichern der Änderungen
        /// </summary>
        public void Save()
        {
            context.SaveChanges();

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            _disposed = true;
        }

        public void DeleteDatabase()
        {
            context.Database.EnsureDeleted();
        }

        public void MigrateDatabase()
        {
            try
            {
                context.Database.Migrate();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        
    }
}



