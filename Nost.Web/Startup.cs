﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nost.Persistence;
using Nost.Core.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Nost.Web.Data;
using Nost.Web.Models;
using Novell.Directory.Ldap;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace Nost.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
               options.UseSqlServer(Configuration.GetConnectionString("DefaultIdentityConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<ILdapConnection, LdapConnection>(p => new LdapConnection() { SecureSocketLayer = true });
            services.AddTransient<IUnitOfWork, UnitOfWork>(p => new UnitOfWork());
            services.AddMvc();
            services.AddCors();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider provider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            InitRoles(provider);
            InitRootUsers(provider);

        }

        public async void InitRoles(IServiceProvider provider)
        {
            using (var scope = provider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                if (roleManager.RoleExistsAsync("Admin") != null)
                    await roleManager.CreateAsync(new IdentityRole("Admin"));

                if (roleManager.RoleExistsAsync("Teacher") != null)
                    await roleManager.CreateAsync(new IdentityRole("Teacher"));

                if (roleManager.RoleExistsAsync("Student") != null)
                    await roleManager.CreateAsync(new IdentityRole("Student"));
            }
        }

        public async void InitRootUsers(IServiceProvider provider)
        {
            using (var scope = provider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

                if (userManager.Users.Where(u => u.UserName == "rootAdmin").FirstOrDefault() == null)
                {
                    await userManager.CreateAsync(new ApplicationUser { UserName = "rootAdmin", SecurityRole = Security.SecurityRole.ADMIN }, "Nost2018!"); //ZrT4?6Lw

                    var user = userManager.Users.Where(u => u.UserName == "rootAdmin").FirstOrDefault();

                    if (user != null && !await userManager.IsInRoleAsync(user, "Admin"))
                    {
                        var res = await userManager.AddToRoleAsync(user, "Admin");
                        Console.WriteLine("add to role: " + res);
                    }
                    //AssignRole(userManager, "rootAdmin", "Admin");
                }

                if (userManager.Users.Where(u => u.UserName == "rootTeacher").FirstOrDefault() == null)
                {
                    await userManager.CreateAsync(new ApplicationUser { UserName = "rootTeacher", SecurityRole = Security.SecurityRole.TEACHER }, "Nost!Lehrer31");
                    //AssignRole(userManager, "rootTeacher", "Teacher");
                    var user = userManager.Users.Where(u => u.UserName == "rootTeacher").FirstOrDefault();

                    if (user != null && !await userManager.IsInRoleAsync(user, "Teacher"))
                    {
                        var res = await userManager.AddToRoleAsync(user, "Teacher");
                        Console.WriteLine("add to role: " + res);
                    }
                }

                //if (userManager.Users.Where(u => u.UserName == "rootStudent").FirstOrDefault() == null)
                //    await userManager.CreateAsync(new ApplicationUser { UserName = "rootStudent", SecurityRole = Security.SecurityRole.STUDENT }, "gn4UV=bX");
            }

        }

        //private async void AssignRole(UserManager<ApplicationUser> userManager, string userName, string roleName)
        //{
        //    var user = userManager.Users.Where(u => u.UserName == userName).FirstOrDefault();

        //    if (user != null && !await userManager.IsInRoleAsync(user, roleName))
        //    {
        //        var res = await userManager.AddToRoleAsync(user, roleName);
        //        Console.WriteLine("add to role: " +res);
        //    }
        //}
    }
}
