﻿using Nost.Core.Entities;
using Nost.Web.Models.Admin;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Nost.Web.Helper
{
    public static class ExamHelper
    {
        public static bool HasExpired(Exam exam,List<ExamAppointment> appointments)
        {
            return GetExpireDate(exam, appointments) >= DateTime.Today;
        }

        public static DateTime GetExpireDate(Exam exam, List<ExamAppointment> appointments)
        {
            return appointments.Where(a => a.Id == exam.AppointmentId).First().ExpireDate;
        }
    }
}
