﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Nost.Core.Contracts;
using Nost.Core.Entities;
using Nost.Logic;
using Nost.Web.Models;
using Nost.Web.Models.AccountViewModels;
using Nost.Web.Models.Admin;
using Novell.Directory.Ldap;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Nost.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        IUnitOfWork uow;
        ILdapConnection connection;
        private readonly ILogger _logger;

        public AdminController(IUnitOfWork uow, ILdapConnection connection, UserManager<ApplicationUser> userManager,
            ILogger<AccountController> logger)
        {
            this.uow = uow;
            this.connection = connection;
            _userManager = userManager;
            _logger = logger;
        }


        public IActionResult Index()
        {
            return View();
        }

        public IActionResult RefreshData()
        {
            LoginViewModel model = new LoginViewModel() { IsValid = true };

            return View(model);
        }

        [HttpPost]
        public IActionResult RefreshData(LoginViewModel model)
        {
            try
            {
                connection.Connect(AccountController.LDAP_URL, AccountController.LDAP_PORT);
                connection.Bind(model.Username + AccountController.LDAP_SUFFIX, model.Password);

                UpdateTeacher();
                UpdateStudents();
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                model.IsValid = false;
                return View(model);
            }
            return View(nameof(Index));
        }

        public IActionResult UpdateTeacherArchived(int teacherId)
        {
            if (uow.TeacherRepository.GetById(teacherId) != null)
            {
                Teacher teacher = uow.TeacherRepository.GetById(teacherId);
                teacher.Archived = !teacher.Archived;
                uow.TeacherRepository.Update(teacher);
                uow.Save();
                _logger.LogInformation($"Teacher {teacher.DisplayName} archiving = {teacher.Archived}");
            }
            return RedirectToAction(nameof(TeacherList));
        }


        #region Subject
        public IActionResult SubjectList()
        {
            SubjectViewModel model = new SubjectViewModel(uow);
            return View(model);
        }

        [HttpPost]
        public IActionResult CreateSubject(SubjectViewModel model)
        {
            if (ModelState.IsValid)
            {
                uow.SubjectRepository.Insert(model.NewSubject);
                uow.Save();
            }
            Department dep = uow.DepartmentRepository.GetById(model.NewSubject.DepartmentId);
            ModelState.Clear();
            model.Init(uow);
            model.NewSubject = new Subject();
            model.NewSubject.Department = dep;
            model.NewSubject.DepartmentId = dep.Id;
            TryValidateModel(model);
            return View(nameof(SubjectList), model);
        }



        public IActionResult EditSubject(int editSubjectId)
        {
            SubjectViewModel model = new SubjectViewModel(uow);
            model.EditedSubject = uow.SubjectRepository.GetById(editSubjectId);

            return View(nameof(SubjectList), model);
        }

        [HttpPost]
        public IActionResult EditSubject(SubjectViewModel model)
        {
            ModelState.Clear();
            TryValidateModel(model.EditedSubject);
            if (ModelState.IsValid)
            {
                uow.SubjectRepository.Update(model.EditedSubject);
                uow.Save();
                Department dep = uow.DepartmentRepository.GetById(model.EditedSubject.DepartmentId);
                ModelState.Clear();
                model = new SubjectViewModel(uow);
                model.NewSubject = new Subject();
                model.NewSubject.Department = dep;
                model.NewSubject.DepartmentId = dep.Id;
                TryValidateModel(model);

                return View(nameof(SubjectList), model);
            }
            return View(nameof(SubjectList), model);
        }

        public IActionResult DeleteSubject(int subjectId)
        {
            Department dep = uow.DepartmentRepository.GetById(uow.SubjectRepository.GetById(subjectId).DepartmentId);
            if (ModelState.IsValid)
            {
                if (uow.ExamRepository.Get(f => f.SubjectId == subjectId).Count() == 0)
                {
                    uow.SubjectRepository.Delete(subjectId);
                    uow.Save();
                }
                else
                {
                    Subject subject = uow.SubjectRepository.GetById(subjectId);
                    if (subject != null)
                    {
                        subject.Archived = true;
                        uow.SubjectRepository.Update(subject);
                        uow.Save();
                    }
                }
            }
            SubjectViewModel model = new SubjectViewModel(uow);
            if (dep != null)
            {
                model.NewSubject = new Subject();
                model.NewSubject.Department = dep;
                model.NewSubject.DepartmentId = dep.Id;
            }
            return View(nameof(SubjectList), model);
        }
        #endregion

        public IActionResult TeacherList()
        {
            TeacherListViewModel model = new TeacherListViewModel();
            model.Init(uow);

            return View(model);
        }


        #region Department

        public IActionResult DepartmentList()
        {
            DepartmentViewModel model = new DepartmentViewModel();
            model.Init(uow);

            return View(model);
        }


        [HttpPost]
        public IActionResult CreateDepartment(DepartmentViewModel model)
        {
            if (ModelState.IsValid)
            {
                uow.DepartmentRepository.Insert(model.NewDepartment);
                uow.Save();
                return RedirectToAction(nameof(DepartmentList));
            }
            model.Init(uow);
            return View(nameof(DepartmentList), model);
        }



        public IActionResult EditDepartment(int editDepartmentId)
        {
            DepartmentViewModel model = new DepartmentViewModel();
            model.Init(uow);

            model.EditedDepartment = uow.DepartmentRepository.GetById(editDepartmentId);

            return View(nameof(DepartmentList), model);
        }

        [HttpPost]
        public IActionResult EditDepartment(DepartmentViewModel model)
        {
            ModelState.Clear();
            TryValidateModel(model.EditedDepartment);
            if (ModelState.IsValid)
            {
                uow.DepartmentRepository.Update(model.EditedDepartment);
                uow.Save();
                return RedirectToAction(nameof(DepartmentList));
            }
            model.Init(uow);
            return View(nameof(DepartmentList), model);
        }


        public IActionResult DeleteDepartment(int departmentId)
        {
            if (ModelState.IsValid)
            {
                if (uow.ExamRepository.Get(f => f.SubjectId == departmentId).Count() == 0
                    && uow.SubjectRepository.Get(f => f.DepartmentId == departmentId).Count() == 0)
                {
                    uow.DepartmentRepository.Delete(departmentId);
                    uow.Save();
                }
            }
            return RedirectToAction(nameof(DepartmentList));
        }
        #endregion

        public IActionResult RegisteredExamList()
        {
            List<Exam> exams = uow.ExamRepository.Get(
                filter: f => f.Appointment.Archived == false,
                orderBy: o => o.OrderBy((u) => u.Student.Sn)
                .ThenBy(e => e.SubjectId)
                .ThenBy(e => e.DepartmentId),
                includeProperties: $"{nameof(Exam.Student)},{nameof(Exam.Subject)},{nameof(Exam.Teacher)},{nameof(Exam.SemesterEnd)},{nameof(Exam.Appointment)}"
                ).ToList();
            return View(exams);
        }

        public IActionResult FilteredRegisteredExamList(int examAppointmentId)
        {
            List<Exam> exams = uow.ExamRepository.Get(
                filter: f => f.Appointment.Archived == false && f.AppointmentId == examAppointmentId,
                orderBy: o => o.OrderBy((u) => u.Student.Sn)
                .ThenBy(e => e.SubjectId)
                .ThenBy(e => e.DepartmentId),
                includeProperties: $"{nameof(Exam.Student)},{nameof(Exam.Subject)},{nameof(Exam.Teacher)},{nameof(Exam.SemesterEnd)},{nameof(Exam.Appointment)}"
                ).ToList();
            return View(nameof(RegisteredExamList), exams);
        }

        public IActionResult SemesterEndList()
        {
            SemesterEndListViewModel model = new SemesterEndListViewModel();
            model.Init(uow);
            return View(model);
        }

        public IActionResult ExamAppointmentList()
        {
            ExamAppointmentViewModel model = new ExamAppointmentViewModel();
            model.Init(uow);

            return View(model);
        }

        [HttpPost]
        public IActionResult CreateExamAppointment(ExamAppointmentViewModel model)
        {
            if (ModelState.IsValid)
            {
                uow.ExamAppointmentRepository.Insert(model.NewExamAppointment);
                uow.Save();
                return RedirectToAction(nameof(ExamAppointmentList));
            }
            model.Init(uow);
            return View(nameof(ExamAppointmentList), model);
        }


        [HttpPost]
        public IActionResult CreateSemesterEnd(SemesterEndListViewModel model)
        {
            if (ModelState.IsValid)
            {
                uow.SemesterEndRepository.Insert(model.NewSemesterEnd);
                uow.Save();
                return RedirectToAction(nameof(SemesterEndList));
            }
            model.Init(uow);
            return View(nameof(SemesterEndList), model);
        }

        public IActionResult EditSemesterEnd(int semesterEndId)
        {
            SemesterEndListViewModel model = new SemesterEndListViewModel();
            model.Init(uow);
            model.EditSemesterEnd = uow.SemesterEndRepository.GetById(semesterEndId);
            return View(nameof(SemesterEndList), model);
        }


        [HttpPost]
        public IActionResult EditSemesterEnd(SemesterEndListViewModel model)
        {
            if (ModelState.IsValid)
            {
                uow.SemesterEndRepository.Update(model.EditSemesterEnd);
                uow.Save();
                return RedirectToAction(nameof(SemesterEndList));
            }
            model.Init(uow);
            return View(nameof(SemesterEndList), model);
        }

        public IActionResult DeleteSemesterEnd(int semesterEndId)
        {
            if (ModelState.IsValid)
            {
                if (uow.ExamRepository.Get(f => f.SemesterEndId == semesterEndId).Count() == 0)
                {
                    //No exams with this semesterend
                    uow.SemesterEndRepository.Delete(semesterEndId);
                    uow.Save();
                }
            }
            return RedirectToAction(nameof(SemesterEndList));
        }

        public IActionResult DeleteExamAppointment(int examAppointmentId)
        {
            if (ModelState.IsValid)
            {
                ExamAppointment examAppointment = uow.ExamAppointmentRepository.GetById(examAppointmentId);
                if (uow.ExamRepository.Get(f => f.Appointment.ExamAppointmentDate == examAppointment.ExamAppointmentDate,
                    includeProperties: $"{nameof(Exam.Appointment)}").Count() == 0)
                {
                    //KEINE Prüfungen + Zeit egal
                    uow.ExamAppointmentRepository.Delete(examAppointmentId);
                    uow.Save();
                }
                else
                {
                    if (examAppointment.ExamAppointmentDate < DateTime.Today)
                    {
                        //Anmeldungen vorhanden + Alt
                        var exams = uow.ExamRepository.Get(f => f.Appointment.ExamAppointmentDate == examAppointment.ExamAppointmentDate,
                        includeProperties: $"{nameof(Exam.Appointment)}").ToList();

                        foreach (var exam in exams)
                        {
                            uow.ExamRepository.Delete(exam);
                        }
                        uow.ExamAppointmentRepository.Delete(examAppointment);
                        uow.Save();
                    }
                    else
                    {
                        ExamAppointmentViewModel model = new ExamAppointmentViewModel();
                        model.Init(uow);
                        model.SelectedDeleteAppointment = examAppointment;
                        return View(nameof(ExamAppointmentList), model);
                    }
                }
            }
            return RedirectToAction(nameof(ExamAppointmentList));
        }

        public IActionResult DeleteSelectedExamAppointment(ExamAppointmentViewModel model)
        {
            ExamAppointment examAppointment = uow.ExamAppointmentRepository.GetById(model.SelectedDeleteAppointment.Id);

            //Anmeldungen vorhanden + Neu
            examAppointment.Archived = true;
            uow.ExamAppointmentRepository.Update(examAppointment);
            uow.Save();
            return RedirectToAction(nameof(ExamAppointmentList));
        }

        public IActionResult EditExamAppointment(int examAppointmentId)
        {
            ExamAppointmentViewModel model = new ExamAppointmentViewModel();
            model.Init(uow);

            model.EditAppointment = uow.ExamAppointmentRepository.GetById(examAppointmentId);

            return View(nameof(ExamAppointmentList), model);
        }

        [HttpPost]
        public IActionResult EditExamAppointment(ExamAppointmentViewModel model)
        {
            if (ModelState.IsValid)
            {
                uow.ExamAppointmentRepository.Update(model.EditAppointment);
                uow.Save();
                return RedirectToAction(nameof(ExamAppointmentList));
            }
            model.Init(uow);
            return View(nameof(ExamAppointmentList), model);
        }

        [HttpGet]
        public IActionResult DeleteExam(int deleteId)
        {
            uow.ExamRepository.Delete(deleteId);
            uow.Save();

            return RedirectToAction(nameof(RegisteredExamList));
        }

        [HttpPost]
        public IActionResult DeleteOldExams(DateTime deleteToDate)
        {
            if (deleteToDate < DateTime.Today)
            {
                Console.WriteLine(deleteToDate);
                uow.ExamRepository.Delete(uow.ExamRepository.Get(x => x.Appointment.ExamAppointmentDate < deleteToDate,
                    includeProperties: $"{nameof(Exam.Appointment)}"));

                uow.ExamAppointmentRepository.Delete(uow.ExamAppointmentRepository.Get(f => f.ExamAppointmentDate < deleteToDate));
                uow.Save();
            }
            else
            {
                ModelState.AddModelError("DeleteOldExams", "Es können nur vergangene Termine gelöscht werden");
            }

            ExamAppointmentViewModel model = new ExamAppointmentViewModel();
            model.Init(uow);
            return View(nameof(ExamAppointmentList), model);
        }

        public void UpdateTeacher()
        {
            List<Teacher> teacherInDb;
            List<Teacher> teacherInLdap = new List<Teacher>();


            Console.WriteLine("--- Update Teacher start ---");


            var searchBase = "OU=Teachers,OU=HTL,DC=EDU,DC=HTL-LEONDING,DC=AC,DC=AT";
            var filter = "cn=*";
            String[] attr = new String[] { "displayName", "cn" };
            var search = connection.Search(searchBase, LdapConnection.SCOPE_SUB, filter, attr, false);
            while (search.hasMore())
            {
                var nextEntry = search.next();

                teacherInLdap.Add(new Teacher()
                {
                    Cn = (nextEntry.getAttributeSet().getAttribute(attr[1]) as LdapAttribute).StringValue,
                    DisplayName = (nextEntry.getAttributeSet().getAttribute(attr[0]) as LdapAttribute).StringValue
                });
            }

            teacherInDb = uow.TeacherRepository.Get().ToList();

            try
            {
                foreach (Teacher tLdap in teacherInLdap)
                {
                    if (teacherInDb.Where(tDb => tDb.Cn.Equals(tLdap.Cn)).Count() == 0) //Teacher is not in DB
                    {
                        uow.TeacherRepository.Insert(tLdap);
                        Console.WriteLine($"+ {tLdap.Cn}");
                    }
                    else
                    {
                        Console.WriteLine($"~ {tLdap.Cn}");
                    }
                }

                //Delete Teacher if archived and no exams with this teacher
                foreach (var teacher in teacherInDb)
                {
                    if (teacher.Archived)
                    {
                        if (uow.ExamRepository.Get(f => f.TeacherId == teacher.Id).Count() == 0)
                        {
                            uow.TeacherRepository.Delete(teacher);
                            Console.WriteLine($"- {teacher.Cn}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
                throw;
            }
            uow.Save();

            Console.WriteLine("--- Update Teacher finished ---");

        }


        public void UpdateStudents()
        {
            List<Student> studentsInDb;
            List<Student> studentsInLdap = new List<Student>();


            Console.WriteLine("--- Update Students start ---");
            var classes = GetSchoolClasses();

            foreach (var schoolClass in classes)
            {
                var searchBase = $"{schoolClass},OU=Students,OU=HTL,DC=EDU,DC=HTL-LEONDING,DC=AC,DC=AT";
                var filter = $"cn=*";
                String[] attr = { "dn", "displayName", "name" };
                var search = connection.Search(searchBase, LdapConnection.SCOPE_SUB, filter, attr, false);

                while (search.hasMore())
                {
                    var nextEntry = search.next();

                    if (nextEntry.DN.Contains("Students"))
                    {
                        var entries = nextEntry.DN.Split(',');
                        string[] displayName = (nextEntry.getAttributeSet().getAttribute(attr[1]) as LdapAttribute).StringValue.Split(' ', StringSplitOptions.RemoveEmptyEntries);

                        string username = (nextEntry.getAttributeSet().getAttribute(attr[2]) as LdapAttribute).StringValue.ToLower();
                        string lastName = displayName[0];
                        string firstName = displayName[1];
                        string department = entries[2].Split('=')[1];
                        string currentClass = entries[1].Split('=')[1];

                        studentsInLdap.Add(new Student()
                        {
                            UserName = username,
                            GivenName = firstName,
                            Sn = lastName,
                            Department = department,
                            SchoolClass = currentClass
                        });
                    }

                }
            }

            studentsInDb = uow.StudentRepository.Get().ToList();

            try
            {
                foreach (Student tLdap in studentsInLdap)
                {
                    if (studentsInDb.Where(tDb => tDb.UserName.Equals(tLdap.UserName)).Count() == 0) //Student is not in DB
                    {
                        uow.StudentRepository.Insert(tLdap);
                        Console.WriteLine($"+ {tLdap.UserName}");
                    }
                    else
                    {
                        var student = uow.StudentRepository.Get(t => t.UserName == tLdap.UserName).FirstOrDefault();
                        student.Sn = tLdap.Sn;
                        student.GivenName = tLdap.GivenName;
                        student.SchoolClass = tLdap.SchoolClass;
                        student.Department = tLdap.Department;
                        uow.StudentRepository.Update(student);
                        Console.WriteLine($"~ {tLdap.UserName}");
                    }
                }

                List<Student> dbStudentsNotInLdap = uow.StudentRepository.Get(s => !studentsInLdap.Any(s2 => s2.UserName == s.UserName)).ToList();

                foreach (Student s in dbStudentsNotInLdap)
                {
                    if (uow.ExamRepository.Get(f => f.StudentId == s.Id).Count() == 0)
                    {
                        uow.StudentRepository.Delete(s);
                        Console.WriteLine($"Delete Student {s.Id} {s.FullName} {s.SchoolClass}");
                    }

                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
                throw;
            }
            uow.Save();

            Console.WriteLine("--- Update Students finished ---");

        }

        public List<string> GetSchoolClasses()
        {
            List<string> classes = new List<string>();

            var searchBase = "OU=Students,OU=HTL,DC=EDU,DC=HTL-LEONDING,DC=AC,DC=AT";
            var filter = "ou=*";
            String[] attr = new String[] { "ou" };
            var search = connection.Search(searchBase, LdapConnection.SCOPE_SUB, filter, attr, false);

            string curDep = "";
            while (search.hasMore())
            {
                var nextEntry = search.next();

                string name = (nextEntry.getAttributeSet().getAttribute(attr[0]) as LdapAttribute).StringValue;
                if (name != "Students")
                {
                    if (name.Length >= 5 && name.Length <= 7)
                    {
                        curDep = nextEntry.DN.Split(',')[1];
                        classes.Add($"OU={name},{curDep}");
                    }
                }
            }
            return classes;
        }

        public IActionResult DownloadCsv(bool getAll = false)
        {
            List<Exam> examList;
            if (getAll)
            {
                examList = uow.ExamRepository.Get(includeProperties: $"Student,Teacher,Subject,SemesterEnd,{nameof(Exam.Appointment)}").ToList();
            }
            else
            {
                examList = uow.ExamRepository.Get(includeProperties: $"Student,Teacher,Subject,SemesterEnd,{nameof(Exam.Appointment)}", filter: x => x.Appointment.ExamAppointmentDate >= DateTime.Today && x.Appointment.Archived == false).ToList();
            }
            var csv = new StringBuilder();

            //csv Header
            csv.AppendLine(ReflectionExtensions.GetDisplayNameProperty<Student>(x => x.UserName) + ";"
                     + ReflectionExtensions.GetDisplayNameProperty<Student>(x => x.GivenName) + ";"
                     + ReflectionExtensions.GetDisplayNameProperty<Student>(x => x.Sn) + ";"
                     + ReflectionExtensions.GetDisplayNameProperty<Student>(x => x.SchoolClass) + ";"
                     + ReflectionExtensions.GetDisplayNameProperty<Department>(x => x.DepartmentName) + ";"
                     + ReflectionExtensions.GetDisplayNameProperty<Subject>(x => x.SubjectName) + ";"
                     + ReflectionExtensions.GetDisplayNameProperty<Exam>(x => x.Repetition) + ";"
                     + ReflectionExtensions.GetDisplayNameProperty<Exam>(x => x.Appointment) + ";"
                     + ReflectionExtensions.GetDisplayNameProperty<Teacher>(x => x.DisplayName) + ";"
                     + ReflectionExtensions.GetDisplayNameProperty<Exam>(x => x.FailedTerm) + ";"
                     + ReflectionExtensions.GetDisplayNameProperty<Exam>(x => x.FailedClass) + ";"
                     + ReflectionExtensions.GetDisplayNameProperty<SemesterEnd>(x => x.SemesterEndDate) + ";"
                     + ReflectionExtensions.GetDisplayNameProperty<Exam>(x => x.IsPcExam) + ";"
                     + ReflectionExtensions.GetDisplayNameProperty<Exam>(x => x.Software) + ";"
                     );

            //csv Data
            foreach (var exam in examList)
            {
                string repetitionType;
                switch (exam.Repetition)
                {
                    case 0:
                        repetitionType = "Semesterprüfung";
                        break;
                    case 1:
                        repetitionType = "1. Wiederholung";
                        break;
                    case 2:
                        repetitionType = "2. Wiederholung";
                        break;
                    default:
                        repetitionType = "Semesterprüfung";
                        break;
                }

                csv.AppendLine(exam.Student.UserName + ";"
                    + exam.Student.GivenName + ";"
                    + exam.Student.Sn + ";"
                    + exam.Student.SchoolClass + ";"
                    + exam.Student.Department + ";"
                    + exam.Subject.SubjectName + ";"
                    + repetitionType + ";"
                    + exam.Appointment.ExamAppointmentDate.ToShortDateString() + ";"
                    + exam.Teacher.DisplayName + ";"
                    + exam.FailedTerm + ";"
                    + exam.FailedClass + ";"
                    + exam.SemesterEnd?.SemesterEndDate.ToShortDateString() + ";"
                    + (exam.IsPcExam ? "Ja" : "Nein") + ";"
                    + exam.Software
                    );
            }
            return File(new UnicodeEncoding().GetBytes(csv.ToString()), "text/csv", $"Report{DateTime.Now.ToString("ddMMyyyy_hhmmss")}.csv");
        }

        public IActionResult Settings()
        {
            SettingsViewModel model = new SettingsViewModel();
            model.Load();
            return View(model);
        }

        [HttpPost]
        public IActionResult Settings(SettingsViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Persist();
                return RedirectToAction(nameof(Index));
            }
            model.Load();
            return View(model);
        }

        public async Task<IActionResult> CreateExamForStudent(int studentId)
        {
            var user = await GetApplicationUser();
            CreateExamForStudentViewModel model = new CreateExamForStudentViewModel();
            model.Exam.Student = uow.StudentRepository.GetById(studentId);
            model.Exam.StudentId = model.Exam.Student.Id;
            model.initModel(uow, user);
            if (model.Exam.Student != null)
                return View(model);
            else
                return RedirectToAction(nameof(SelectStudent));
        }

        [HttpPost]
        public IActionResult CreateExamForStudent(CreateExamForStudentViewModel model)
        {

            Student student = uow.StudentRepository.GetById(model.Exam.StudentId);
            if (student != null)
            {
                model.Exam.DepartmentId = uow.DepartmentRepository.Get(f => f.DepartmentShortcut == student.Department).FirstOrDefault().Id;

                //ModelState.Clear();
                //bool state = TryValidateModel(model.Exam);

                if (ModelState.IsValid)
                {
                    Teacher dbTeacher = uow.TeacherRepository.GetById(model.Exam.TeacherId);
                    if ($"{dbTeacher.DisplayName} ({dbTeacher.Cn})" == model.Exam.Teacher.DisplayName)
                    {
                        if (uow.ExamRepository.Count(x => x.AppointmentId == model.Exam.AppointmentId && x.SubjectId == model.Exam.SubjectId && x.FailedTerm == model.Exam.FailedTerm && x.StudentId == model.Exam.StudentId) > 0)
                        {
                            ModelState.AddModelError("Exam", "Sie sind bei diesem Termin bereits in diesem Gegenstand angemeldet");
                        }
                        else if (uow.ExamRepository.Count(x => x.AppointmentId == model.Exam.AppointmentId && x.StudentId == model.Exam.StudentId) >= 4)
                        {
                            ModelState.AddModelError("Exam.AppointmentId", "Sie sind bei diesem Termin bereits 4x angemeldet");
                        }
                        else
                        {
                            model.Exam.Department = uow.DepartmentRepository.GetById(model.Exam.DepartmentId);
                            model.Exam.FailedClass = model.Exam.FailedClass.ToUpper();

                            uow.ExamRepository.Insert(model.Exam);
                            uow.Save();


                            return RedirectToAction(nameof(RegisteredExamList));
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("Exam.TeacherId", "Dieser Lehrer existiert nicht. Wählen Sie einen Lehrer aus der Liste aus");
                    }
                }

                if (!ModelState.IsValid)
                {
                    foreach (var key in ModelState.Keys)
                        foreach (var item in ModelState[key].Errors)
                        {
                            _logger.LogCritical($"{key} {item.ErrorMessage}");
                        }
                }
                model.initModel(uow, GetApplicationUser().Result);
                model.Exam.StudentId = student.Id;
                model.Exam.Student = student;
                return View(nameof(CreateExamForStudent), model);
            }
            else
            {
                ModelState.AddModelError("Exam.StudentId", "Es wurde kein Schüler ausgewählt");
            }
            model.initModel(uow, GetApplicationUser().Result);
            return View(model);

        }

        [HttpPost]
        public void UpdateSubjectList([FromBody]CreateExamForStudentViewModel model)
        {
            model.Subjects = new SelectList(uow.SubjectRepository.Get(filter: s => !s.Archived && s.Department.DepartmentShortcut == model.Exam.Student.Department,
                orderBy: x => x.OrderBy(y => y.DepartmentId).ThenBy(y => y.SubjectName)).ToList()
                    .Select(s => new SelectListItem
                    {
                        Text = s.SubjectName,
                        Value = s.Id.ToString(),
                    }), nameof(SelectListItem.Value), nameof(SelectListItem.Text));
        }


        [HttpGet]
        public JsonResult GetStudents(string term)
        {
            return Json(uow.StudentRepository.Get(
                filter: t => String.IsNullOrEmpty(term) || term.Equals("*") || String.IsNullOrWhiteSpace(term) ?
                true : t.FullName.ToLower().Contains(term.ToLower()) || t.SchoolClass.ToLower().Contains(term.ToLower()),
                orderBy: o => o.OrderBy(b => b.GivenName).ThenBy(n => n.SchoolClass))
                .Select(a => new { label = $"{a.FullName} {a.SchoolClass}", id = a.Id }).Take(40));
        }

        public async Task<ApplicationUser> GetApplicationUser()
        {
            return await _userManager.GetUserAsync(User);
            //return new ApplicationUser() { UserName = "in130082", Department = "IF" };
        }

        public IActionResult DownloadPdf(int examId)
        {
            Exam exam = uow.ExamRepository.Get(e => e.Id == examId, includeProperties: $"{nameof(Exam.Subject)},{nameof(Exam.Teacher)},{nameof(Exam.Student)},{nameof(Exam.SemesterEnd)},{nameof(Exam.Appointment)}").FirstOrDefault();
            if (exam != null && GetApplicationUser().Result.SecurityRole == Security.SecurityRole.ADMIN)
            {
                DocumentBuilder documentBuilder = new DocumentBuilder();
                string path = documentBuilder.CreatePdfOfExamForRegistration(exam);
                var document = new FileInfo(path);
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = document.Name,

                    // always prompt the user for downloading, set to true if you want 
                    // the browser to try to show the file inline
                    Inline = false,
                };
                Response.Headers.Add("Content-Disposition", cd.ToString());
                return File(System.IO.File.ReadAllBytes(path), MediaTypeNames.Application.Pdf);
            }
            else
            {
                return RedirectToAction(nameof(RegisteredExamList));
            }
        }


        public IActionResult DownloadExamAppointmentPdf(int examAppointmentId)
        {
            List<Exam> exams = uow.ExamRepository.Get(e => e.AppointmentId == examAppointmentId, includeProperties: $"{nameof(Exam.Subject)},{nameof(Exam.Teacher)},{nameof(Exam.Student)},{nameof(Exam.SemesterEnd)},{nameof(Exam.Appointment)},{nameof(Exam.Department)}").ToList();

            if (exams != null && exams.Count > 0 && GetApplicationUser().Result.SecurityRole != Security.SecurityRole.STUDENT)
            {
                DocumentBuilder documentBuilder = new DocumentBuilder();
                string path = documentBuilder.CreatePdfForExamAppointment(uow.ExamAppointmentRepository.GetById(examAppointmentId), exams);
                var document = new FileInfo(path);
                var cd = new ContentDisposition
                {
                    FileName = document.Name,

                    // always prompt the user for downloading, set to true if you want 
                    // the browser to try to show the file inline
                    Inline = false,
                };
                Response.Headers.Add("Content-Disposition", cd.ToString());
                return File(System.IO.File.ReadAllBytes(path), MediaTypeNames.Application.Pdf);
            }
            else
            {
                return RedirectToAction(nameof(RegisteredExamList));
            }
        }

        public IActionResult DownloadSingleExamAppointmentPdf(int examId)
        {
            List<Exam> exams = uow.ExamRepository.Get(e => e.Id == examId, includeProperties: $"{nameof(Exam.Subject)},{nameof(Exam.Teacher)},{nameof(Exam.Student)},{nameof(Exam.SemesterEnd)},{nameof(Exam.Appointment)},{nameof(Exam.Department)}").ToList();

            if (exams != null && exams.Count > 0 && GetApplicationUser().Result.SecurityRole != Security.SecurityRole.STUDENT)
            {
                DocumentBuilder documentBuilder = new DocumentBuilder();
                string path = documentBuilder.CreatePdfForExamAppointment(uow.ExamAppointmentRepository.GetById(exams[0].AppointmentId), exams);
                var document = new FileInfo(path);
                var cd = new ContentDisposition
                {
                    FileName = document.Name,

                    // always prompt the user for downloading, set to true if you want 
                    // the browser to try to show the file inline
                    Inline = false,
                };
                Response.Headers.Add("Content-Disposition", cd.ToString());
                return File(System.IO.File.ReadAllBytes(path), MediaTypeNames.Application.Pdf);
            }
            else
            {
                return RedirectToAction(nameof(RegisteredExamList));
            }
        }


        public IActionResult SelectStudent()
        {
            return View(new Student());
        }

        [HttpPost]
        public IActionResult SelectStudent(Student student)
        {
            if (student.Id <= 0)
            {
                ModelState.AddModelError("Id", "Bitte wählen sie einen Schüler aus");
            }
            TryValidateModel(student);

            if (ModelState.IsValid)
            {
                return RedirectToAction(nameof(CreateExamForStudent), new { studentId = student.Id });
            }
            return View(student);
        }
    }
}