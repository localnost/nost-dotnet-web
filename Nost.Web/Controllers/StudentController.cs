﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nost.Web.Models;
using Nost.Core.Contracts;
using Nost.Logic;
using System.IO;
using System.Net.Mime;
using Nost.Core.Entities;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Novell.Directory.Ldap;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Nost.Web.Models.Admin;

namespace Nost.Web.Controllers
{

    [Authorize]
    public class StudentController : Controller
    {
        IUnitOfWork uow;
        ILdapConnection connection;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;


        public StudentController(IUnitOfWork uow, ILdapConnection connection, UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<AccountController> logger)
        {
            this.uow = uow;
            this.connection = connection;
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
        }


        public IActionResult Index()
        {
            return RedirectToAction(nameof(Dashboard));
        }

        public async Task<IActionResult> Dashboard()
        {
            ExamOverviewViewModel model = new ExamOverviewViewModel(uow, await GetApplicationUser());
            return View(model);
        }

        [HttpGet]
        public IActionResult DeleteExam(int examId)
        {
            Exam exam = uow.ExamRepository.Get(f=>f.Id == examId, includeProperties: $"{nameof(Exam.Appointment)},{nameof(Exam.Student)}").FirstOrDefault();
            
            if(exam != null //&& exam.Appointment.ExamAppointmentDate.AddDays(settings.ExamLastDeleteDays) >= DateTime.Today
                && exam.Student.UserName == GetApplicationUser().Result.UserName)
            {
                uow.ExamRepository.Delete(examId);
                uow.Save();
            }
            
            return RedirectToAction(nameof(Dashboard));
        }


        [HttpGet]
        public async Task<IActionResult> RegisterExam()
        {
            RegisterExamViewModel model = new RegisterExamViewModel(uow, await GetApplicationUser());
            
            if(model.Subjects.Count() == 0)
            {
                ModelState.AddModelError("Exam.SubjectId","Keine Gegenstände verfügbar");
            }
            if (model.ExamAppointments.Count() == 0)
            {
                ModelState.AddModelError("Exam.AppointmentId", "Keine Prüfungstermine verfügbar. Beachten Sie auch, dass maximal 4 Prüfungen pro Tag gemacht werden können");
            }
            if (model.SemesterEndDates.Count() == 0)
            {
                ModelState.AddModelError("Exam.SemesterEndId", "Keine Zeugnisstage verfügbar");
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> RegisterExam(RegisterExamViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.isHashValid)
                {
                    Teacher dbTeacher = uow.TeacherRepository.GetById(model.Exam.TeacherId);
                    if ($"{dbTeacher.DisplayName} ({dbTeacher.Cn})" == model.Exam.Teacher.DisplayName)
                    {
                        if (uow.ExamRepository.Count(x => x.AppointmentId == model.Exam.AppointmentId && x.SubjectId == model.Exam.SubjectId && x.FailedTerm == model.Exam.FailedTerm && x.StudentId == model.Exam.StudentId) > 0)
                        {
                            ModelState.AddModelError("Exam", "Sie sind bei diesem Termin bereits in diesem Gegenstand angemeldet");
                        }
                        else if (uow.ExamRepository.Count(x => x.AppointmentId == model.Exam.AppointmentId && x.StudentId == model.Exam.StudentId) >= 4)
                        {
                            ModelState.AddModelError("Exam.AppointmentId", "Sie sind bei diesem Termin bereits 4x angemeldet");
                        }
                        else if(uow.ExamAppointmentRepository.GetById(model.Exam.AppointmentId).ExpireDate <= DateTime.Today)
                        {
                            ModelState.AddModelError("Exam.AppointmentId", "Termin ist bereits abgelaufen");
                        }
                        else
                        {
                            model.Exam.Department = uow.DepartmentRepository.GetById(model.Exam.DepartmentId);
                            model.Exam.FailedClass = model.Exam.FailedClass.ToUpper();

                            uow.ExamRepository.Insert(model.Exam);
                            uow.Save();


                            return RedirectToAction(nameof(Dashboard));
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("Exam.TeacherId", "Dieser Lehrer existiert nicht. Wählen Sie einen Lehrer aus der Liste aus");
                    }
                }
                else
                {
                    _logger.LogCritical($"\n{GetApplicationUser().Result.UserName} versuchte die Website zu manipulieren\n");
                }
            }
            
            if(!ModelState.IsValid)
            {
                foreach (var key in ModelState.Keys)
                    foreach (var item in ModelState[key].Errors)
                    {
                        _logger.LogCritical($"{key} {item.ErrorMessage}");
                    }
            }
            model.initModel(uow, await GetApplicationUser());
            
            return View(model);
        }

        [HttpGet]
        public JsonResult GetTeachers(string term)
        {
            return Json(uow.TeacherRepository.Get(filter: t => String.IsNullOrEmpty(term) ||
            String.IsNullOrWhiteSpace(term) ||
            term.Equals("*") ? true : t.DisplayName.ToLower().Contains(term.ToLower()) &&
            !t.Archived).OrderBy(a => a.DisplayName.Split(' ')[0])
                .Select(a => new { label = $"{a.DisplayName} ({a.Cn})", id = a.Id}));
        }

        public IActionResult DownloadPdf(int examId)
        {
            Exam exam = uow.ExamRepository.Get(e => e.Id == examId, includeProperties: $"{nameof(Exam.Subject)},{nameof(Exam.Teacher)},{nameof(Exam.Student)},{nameof(Exam.SemesterEnd)},{nameof(Exam.Appointment)}").FirstOrDefault();

            if (exam != null && exam.Student.UserName == GetApplicationUser().Result.UserName)
            {
                DocumentBuilder documentBuilder = new DocumentBuilder();
                string path = documentBuilder.CreatePdfOfExamForRegistration(exam);
                var document = new FileInfo(path);
                var cd = new ContentDisposition
                {
                    FileName = document.Name,

                    // always prompt the user for downloading, set to true if you want 
                    // the browser to try to show the file inline
                    Inline = false,
                };
                Response.Headers.Add("Content-Disposition", cd.ToString());
                return File(System.IO.File.ReadAllBytes(path), MediaTypeNames.Application.Pdf);
            }
            else
            {
                return RedirectToAction(nameof(Dashboard));
            }
        }

        public async Task<ApplicationUser> GetApplicationUser()
        {
            return await _userManager.GetUserAsync(User);
            //return new ApplicationUser() { UserName = "in130082", Department = "IF" };
        }
    }
}