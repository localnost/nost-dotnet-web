﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Nost.Core.Contracts;
using Nost.Core.Entities;
using Nost.Logic;
using Nost.Web.Models;

namespace Nost.Web.Controllers
{
    [Authorize(Roles = "Teacher")]
    public class TeacherController : Controller
    {
        private IUnitOfWork uow;

        public TeacherController(IUnitOfWork uow)
        {
            this.uow = uow;
        }


        [HttpGet]
        public IActionResult RegisteredExamTeacherList()
        {
            RegisteredExamListViewModel model = new RegisteredExamListViewModel();
            model.GetExams(uow);
            return View(model);
        }

        [HttpPost]
        public IActionResult RegisteredExamTeacherList(RegisteredExamListViewModel model)
        {
            model.filterByTeacher(uow);
            return View(model);
        }


        public IActionResult DownloadExamAppointmentPdfByTeacher(int examId, int teacherFilterId)
        {
            List<Exam> exams = uow.ExamRepository.Get(e => e.AppointmentId == examId && e.TeacherId == teacherFilterId,
                includeProperties: $"{nameof(Exam.Subject)},{nameof(Exam.Teacher)},{nameof(Exam.Student)},{nameof(Exam.SemesterEnd)},{nameof(Exam.Appointment)},{nameof(Exam.Department)}").ToList();

            if (exams != null && exams.Count > 0)
            {
                DocumentBuilder documentBuilder = new DocumentBuilder();
                string path = documentBuilder.CreatePdfForExamAppointment(uow.ExamAppointmentRepository.GetById(exams[0].AppointmentId), exams);
                var document = new FileInfo(path);
                var cd = new ContentDisposition
                {
                    FileName = document.Name,

                    // always prompt the user for downloading, set to true if you want 
                    // the browser to try to show the file inline
                    Inline = false,
                };
                Response.Headers.Add("Content-Disposition", cd.ToString());
                return File(System.IO.File.ReadAllBytes(path), MediaTypeNames.Application.Pdf);
            }
            else
            {
                return RedirectToAction(nameof(RegisteredExamTeacherList));
            }
        }
    }
}