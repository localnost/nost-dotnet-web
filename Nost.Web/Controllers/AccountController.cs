﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Novell.Directory.Ldap;
using System.Linq;
using Nost.Web.Models;
using Nost.Web.Models.AccountViewModels;
using System.Security.Claims;
using Nost.Core.Contracts;
using Nost.Core.Entities;
using Newtonsoft.Json;

namespace Nost.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        public const string LDAP_URL = "addc01.edu.htl-leonding.ac.at";
        public const int LDAP_PORT = 636;
        public const string LDAP_SUFFIX = "@EDU";

        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;
        ILdapConnection con;
        IUnitOfWork uow;


        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<AccountController> logger, ILdapConnection connection, IUnitOfWork uow)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            con = connection;
            this.uow = uow;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ViewData["ReturnUrl"] = returnUrl;
            return View(new LoginViewModel() { IsValid = true });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            model.Username = model.Username.ToLower();
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                string lastName = "";
                string firstName = "";
                string schoolClass = "";
                string department = "";
                bool isTeacher = false;
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true

                if (model.Username.Contains("admin"))
                {
                    returnUrl = "/Admin/Index";
                    await _signInManager.PasswordSignInAsync(model.Username, model.Password, false, lockoutOnFailure: false);
                    _logger.LogInformation("User logged in.");
                    return RedirectToLocal(returnUrl);
                }
                else if (model.Username.Contains("teacher"))
                {
                    returnUrl = "/Teacher/RegisteredExamTeacherList";
                    await _signInManager.PasswordSignInAsync(model.Username, model.Password, false, lockoutOnFailure: false);
                    _logger.LogInformation("User logged in.");
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    bool result = SignInLdap(model.Username, model.Password, ref lastName, ref firstName, ref schoolClass, ref department, ref isTeacher);
                    if (result)
                    {
                        var user = new ApplicationUser { UserName = model.Username.ToLower(), FirstName = firstName, LastName = lastName, SchoolClass = schoolClass, Department = department, SecurityRole = isTeacher ? Security.SecurityRole.TEACHER : Security.SecurityRole.STUDENT };
                        switch (user.SecurityRole)
                        {
                            case Security.SecurityRole.ADMIN:
                                returnUrl = "/Admin";
                                break;
                            case Security.SecurityRole.TEACHER:
                                returnUrl = "/Teacher/Dashboard";
                                break;
                            case Security.SecurityRole.STUDENT:
                                returnUrl = "/Student/Dashboard";
                                break;
                            default:
                                break;
                        }
                        if (_userManager.Users.Where(u => u.UserName == model.Username).FirstOrDefault() == null)
                        {
                            //=>User exisitert noch nicht
                            //User wird angelegt
                            await _userManager.CreateAsync(user);
                        }


                        Student currentStudent = uow.StudentRepository.Get(x => x.UserName == model.Username).FirstOrDefault();
                        if (currentStudent == null)
                        {
                            uow.StudentRepository.Insert(new Student() { SchoolClass = schoolClass, Department = department, GivenName = firstName, Sn = lastName, UserName = model.Username.ToLower() });
                            uow.Save();
                            Console.WriteLine("Persist new Student");
                        }
                        else
                        {
                            //Update Pupil
                            if (currentStudent.SchoolClass == null || currentStudent.SchoolClass.Equals(user.SchoolClass))
                            {
                                currentStudent.Department = user.Department;
                                currentStudent.SchoolClass = user.SchoolClass;
                                uow.StudentRepository.Update(currentStudent);
                                uow.Save();
                            }
                        }

                        //if (isTeacher)
                        //{
                        //    uow.StudentRepository.Insert(new Student() { SchoolClass = "2AHIF(Lehrer)", Department = "Informatik (Lehrer)", GivenName = firstName, Sn = lastName, UserName = model.Username });
                        //    uow.Save();
                        //    //Lehrer-Rolle hinzufügen
                        //    //await _userManager.AddToRoleAsync(user, "Teacher");
                        //    Console.WriteLine("Is Teacher: " + model.Username);
                        //    returnUrl = "/Student/Dashboard/";
                        //}
                        //else
                        //{

                        //Besucher ist normal
                        //await _userManager.AddToRoleAsync(user, "Visitor");
                        //Console.WriteLine("Is Student: " + model.Username);
                        //returnUrl = "/Student/Dashboard/";
                        //}

                        //Der User wird eingeloggt
                        await _signInManager.SignInAsync(_userManager.Users.Where(u => u.UserName == model.Username).FirstOrDefault(), false);
                        _logger.LogInformation("User logged in.");

                        return RedirectToLocal(returnUrl);
                    }
                }

                return View(model);

            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private bool SignInLdap(string userName, string password, ref string lastName, ref string firstName, ref string schoolClass, ref string department, ref bool isTeacher)
        {
            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
            {
                try
                {
                    con.Connect(LDAP_URL, LDAP_PORT);
                    con.Bind(userName + LDAP_SUFFIX, password);

                    var searchBase = "OU=HTL,DC=EDU,DC=HTL-LEONDING,DC=AC,DC=AT";
                    var filter = $"cn={userName}";
                    String[] attr = { "dn", "displayName" };
                    var search = con.Search(searchBase, LdapConnection.SCOPE_SUB, filter, attr, false);
                    while (search.hasMore())
                    {
                        var nextEntry = search.next();
                        Console.WriteLine("Group: " + nextEntry.DN);

                        string[] displayName = (nextEntry.getAttributeSet().getAttribute(attr[1]) as LdapAttribute).StringValue.Split(' ', StringSplitOptions.RemoveEmptyEntries);

                        lastName = displayName[0];
                        firstName = displayName[1];

                        if (nextEntry.DN.Contains("Teachers"))
                        {
                            Console.WriteLine("Teacher logged in");
                            isTeacher = true;
                        }
                        if (nextEntry.DN.Contains("Students"))
                        {
                            var entries = nextEntry.DN.Split(',');
                            Console.WriteLine("Student logged in");
                            schoolClass = entries[1].Split('=')[1];
                            department = entries[2].Split('=')[1];
                        }

                    }

                }
                catch (Exception)
                {
                    //Falls Anmeldedaten nicht gültig sind, Server offline usw..
                    return false;
                }

            }
            return true;
        }

        public static string GetFullDepartmentName(string shortName)
        {
            switch (shortName)
            {
                case "IF":
                    return "Informatik";
                case "BG":
                    return "Medizintechnik";
                case "FE":
                    return "Fachschule Elektronik";
                case "HE":
                    return "Elektronik";
                case "IT":
                    return "Medientechnik";
                case "AD":
                    return "Abendschule";
                case "KD":
                    return "Kolleg";
                default:
                    return shortName;
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Lockout()
        {
            return View();
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }


        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}
