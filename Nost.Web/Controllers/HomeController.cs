﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nost.Web.Models;

namespace Nost.Web.Controllers
{
    public class HomeController : Controller
    {

        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;


        public HomeController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<AccountController> logger)
        {   
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
        }


        [Authorize]
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);

            switch (user.SecurityRole)
            {
                case Security.SecurityRole.ADMIN:
                    return RedirectToAction("Index", "Admin");
                case Security.SecurityRole.TEACHER:
                    return RedirectToAction("Dashboard", "Teacher");
                case Security.SecurityRole.STUDENT:
                    return RedirectToAction("Dashboard", "Student");
                default:
                    return RedirectToAction(nameof(Error));
            }
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
