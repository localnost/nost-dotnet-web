﻿using Nost.Core.Contracts;
using Nost.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nost.Web.Models.Admin
{
    public class TeacherListViewModel
    {
        public List<Teacher> Teachers{ get; set; }

        public bool CheckboxValue{ get; set; }

        public TeacherListViewModel()
        {
        }

        internal void Init(IUnitOfWork uow)
        {
            Teachers = uow.TeacherRepository.Get(orderBy: x => x.OrderBy(y => y.Archived).ThenBy(y => y.DisplayName)).ToList();
            CheckboxValue = false;
        }

        public Teacher GetPrevious(Teacher current)
        {
            try
            {
                var res = Teachers.TakeWhile(x => !x.Equals(current));
                return res.LastOrDefault();
            }
            catch
            {
                return default(Teacher);
            }
        }
    }
}
