﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nost.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nost.Web.Models.Admin
{
    public class CreateExamForStudentViewModel: RegisterExamViewModel
    {

        public CreateExamForStudentViewModel()
        {

        }

        public CreateExamForStudentViewModel(IUnitOfWork uow, ApplicationUser user) : base(uow,user)
        {
        }
    }
}
