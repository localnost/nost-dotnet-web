﻿using Nost.Core.Contracts;
using Nost.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nost.Web.Models.Admin
{
    public class DepartmentViewModel
    {
        public Department NewDepartment { get; set; }
        public List<Department> Departments { get; set; }

        public Department EditedDepartment { get; set; }

        public DepartmentViewModel()
        {
            NewDepartment = new Department();
        }

        internal void Init(IUnitOfWork uow)
        {
            Departments = uow.DepartmentRepository.Get(orderBy: o => o.OrderBy(a => a.DepartmentName)).ToList();
        }
    }
}
