﻿using Nost.Core.Contracts;
using Nost.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nost.Web.Models.Admin
{
    public class SemesterEndListViewModel
    {
        public SemesterEnd NewSemesterEnd { get; set; }

        public List<SemesterEnd> SemesterEnds { get; set; }
        public SemesterEnd EditSemesterEnd { get; set; }

        public SemesterEndListViewModel()
        {
            NewSemesterEnd = new SemesterEnd();
            NewSemesterEnd.SemesterEndDate = DateTime.Today;
        }

        internal void Init(IUnitOfWork uow)
        {
            SemesterEnds = uow.SemesterEndRepository.Get(orderBy: o => o.OrderByDescending(s => s.SemesterEndDate)).ToList();
        }
    }
}
