﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nost.Core.Contracts;
using Nost.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nost.Web.Models.Admin
{
    public class SubjectViewModel
    {
        public Subject NewSubject { get; set; } 
        public List<Subject> Subjects { get; set; }

        public SelectList Departments{ get; set; }

        public Subject EditedSubject { get; set; }

        public SubjectViewModel()
        {
            NewSubject = new Subject();
        }

        public SubjectViewModel(IUnitOfWork uow) : this()
        {
            Init(uow);
        }

        public void Init(IUnitOfWork uow)
        {
            Departments = new SelectList(uow.DepartmentRepository.Get(orderBy: x => x.OrderBy(y => y.DepartmentName)).ToList().Select(d => new SelectListItem
            {
                Text = d.DepartmentName,
                Value = d.Id.ToString(),
            }), nameof(SelectListItem.Value), nameof(SelectListItem.Text));

            Subjects = uow.SubjectRepository.Get(orderBy: o => o.OrderBy(a => a.Archived).ThenBy(b => b.DepartmentId).ThenBy(y => y.SubjectName), includeProperties: $"{nameof(Department)}").ToList();
        }
    }
}
