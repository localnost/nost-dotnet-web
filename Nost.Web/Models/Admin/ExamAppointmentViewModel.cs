﻿using Nost.Core.Contracts;
using Nost.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nost.Web.Models.Admin
{
    public class ExamAppointmentViewModel
    {
        public ExamAppointment NewExamAppointment { get; set; }

        public List<ExamAppointment> ExamAppointments { get; set; }

        public ExamAppointment SelectedDeleteAppointment { get; set; }

        public ExamAppointment EditAppointment { get; set; }

        public ExamAppointmentViewModel()
        {
            NewExamAppointment = new ExamAppointment();
            NewExamAppointment.ExamAppointmentDate = DateTime.Today.AddDays(7);
            NewExamAppointment.ExpireDate = DateTime.Today;
        }
            
        internal void Init(IUnitOfWork uow)
        {
            ExamAppointments = uow.ExamAppointmentRepository.Get(orderBy: o => o.OrderBy(s => s.ExamAppointmentDate)).ToList();
        }
    }
}
