﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Nost.Web.Models.Admin
{
    public class SettingsViewModel
    {
        public int ExamLastDeleteDays { get; set; }

        private const string SETTINGS_FILE = "./settings.dat";

        public void Persist()
        {
            File.WriteAllText(SETTINGS_FILE, ExamLastDeleteDays.ToString());
        }

        public void Load()
        {
            string content = File.ReadAllText(SETTINGS_FILE);

            int examLastDeleteDays = 5;
            Int32.TryParse(content, out examLastDeleteDays);

            ExamLastDeleteDays = examLastDeleteDays;
        }

        public static SettingsViewModel GetSettings()
        {
            var model = new SettingsViewModel();

            model.Load();

            return model;
        }
    }
}
