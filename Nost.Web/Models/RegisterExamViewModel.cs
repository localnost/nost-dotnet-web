﻿using Nost.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nost.Core.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.Text;
using Nost.Web.Helper;

namespace Nost.Web.Models
{
    public class RegisterExamViewModel
    {

        public RegisterExamViewModel()
        {

        }

        public RegisterExamViewModel(IUnitOfWork uow, ApplicationUser user) : this()
        {
            initModel(uow, user);
        }

        [Required]
        public Exam Exam { get; set; } = new Exam();

        public SelectList Subjects { get; set; }

        public SelectList Teachers { get; set; }

        public SelectList ExamAppointments { get; set; }

        public SelectList SemesterEndDates { get; set; }

        public SelectList Departments { get; set; }

        public string Hash { get; set; }


        public void initModel(IUnitOfWork uow, ApplicationUser user)
        {
            if (user.SecurityRole != Security.SecurityRole.ADMIN)
            {
                var student = uow.StudentRepository.Get(filter: x => x.UserName == user.UserName).FirstOrDefault();
                Exam.StudentId = student.Id;
                Exam.Student = student;

                Exam.Department = uow.DepartmentRepository.Get(f => f.DepartmentShortcut == user.Department).FirstOrDefault();
                Exam.DepartmentId = Exam.Department.Id;

                Subjects = new SelectList(uow.SubjectRepository.Get(filter: s => !s.Archived && s.Department.Id == Exam.Department.Id).ToList()
                    .Select(s => new SelectListItem
                    {
                        Text = s.SubjectName,
                        Value = s.Id.ToString(),
                    }), nameof(SelectListItem.Value), nameof(SelectListItem.Text));
            }
            else
            {
                if(Exam.StudentId > 0)
                {
                    Exam.Student = uow.StudentRepository.Get(filter: x => x.Id == Exam.StudentId).FirstOrDefault();
                    Exam.DepartmentId = uow.DepartmentRepository.Get(f => f.DepartmentShortcut == Exam.Student.Department).FirstOrDefault().Id;
                }
                Subjects = new SelectList(uow.SubjectRepository.Get(filter: s => !s.Archived && s.DepartmentId == Exam.DepartmentId,
                   orderBy: x => x.OrderBy(y => y.DepartmentId).ThenBy(y => y.SubjectName)).ToList()
                       .Select(s => new SelectListItem
                       {
                           Text = s.SubjectName,
                           Value = s.Id.ToString(),
                       }), nameof(SelectListItem.Value), nameof(SelectListItem.Text));
            }


            Departments = new SelectList(uow.DepartmentRepository.Get(orderBy: o => o.OrderBy(b => b.DepartmentName)).ToList()
                .Select(s => new SelectListItem
                {
                    Text = s.DepartmentName,
                    Value = s.Id.ToString(),
                    Selected = user.Department == s.DepartmentShortcut
                }), nameof(SelectListItem.Value), nameof(SelectListItem.Text));

            Teachers = new SelectList(uow.TeacherRepository.Get(filter: t => !t.Archived).ToList().Select(t => new SelectListItem()
            {
                Text = $"{t.DisplayName} ({t.Cn})",
                Value = t.Id.ToString()
            }), nameof(SelectListItem.Value), nameof(SelectListItem.Text));


            SemesterEndDates = new SelectList(uow.SemesterEndRepository.Get(f => f.SemesterEndDate <= DateTime.Now.AddMonths(6),
                orderBy: x=>x.OrderBy(y=>y.SemesterEndDate)).ToList().Select(s => new SelectListItem
            {
                Text = String.Format("{0:dd.MM.yyyy}", s.SemesterEndDate),
                Value = s.Id.ToString(),
            }), nameof(SelectListItem.Value), nameof(SelectListItem.Text));

            ExamAppointments = new SelectList(uow.ExamAppointmentRepository.Get(filter: f => f.ExamAppointmentDate >= DateTime.Today && f.Archived == false,
                orderBy: x => x.OrderBy(y => y.ExamAppointmentDate))
                .ToList().Select(s => new SelectListItem
            {
                Text = s.ExamAppointmentDate.ToShortDateString()+ (s.ExpireDate <= DateTime.Today? " -- Frist abgelaufen --":""),
                Value = s.Id.ToString(),
            }), nameof(SelectListItem.Value), nameof(SelectListItem.Text));


            //Hash
            Hash = GetCurrentHash();
        }

        public bool isHashValid
        {
            get
            {
                return Hash == GetCurrentHash();
            }
        }

        public string GetCurrentHash()
        {
            return GetHashString($"{Exam.StudentId}_{Exam.DepartmentId}_!");
        }


        public static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA512.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
    }

}
