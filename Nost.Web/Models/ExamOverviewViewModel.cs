﻿using Nost.Core.Contracts;
using Nost.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nost.Web.Models
{
    public class ExamOverviewViewModel
    {

        public ExamOverviewViewModel(IUnitOfWork uow, ApplicationUser applicationUser)
        {
            init(uow, applicationUser);
        }

        public List<Exam> Exams { get; set; }

        public List<Exam> OldExams { get; set; }

        public List<ExamAppointment> ExamAppointments { get; set; }

        public Student Student { get; set; }

        public bool isStudentsDepartmentExisting { get; set; }

        public void init(IUnitOfWork uow, ApplicationUser user)
        {
            Student = uow.StudentRepository.Get(filter: x => x.UserName == user.UserName).FirstOrDefault();

            Exams = uow.ExamRepository.Get(
                filter: a => a.Student.Id == Student.Id && a.Appointment.ExamAppointmentDate >= DateTime.Today,
                includeProperties: $"{nameof(Student)},{nameof(Teacher)},{nameof(Subject)},{nameof(Department)},{nameof(Exam.Appointment)}")
                .ToList();


            OldExams = uow.ExamRepository.Get(
                filter: a => a.Student.Id == Student.Id && a.Appointment.ExamAppointmentDate < DateTime.Today,
                includeProperties: $"{nameof(Student)},{nameof(Teacher)},{nameof(Subject)},{nameof(Department)},{nameof(Exam.Appointment)}")
                .ToList();

            isStudentsDepartmentExisting = uow.DepartmentRepository.Get(f => f.DepartmentShortcut == Student.Department).Count() > 0;

            ExamAppointments = uow.ExamAppointmentRepository.Get().ToList();
        }

        
    }
}
