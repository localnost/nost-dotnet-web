﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nost.Core.Contracts;
using Nost.Core.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Nost.Web.Models
{
    public class RegisteredExamListViewModel
    {
        public List<Exam> Exams { get; set; }

        public SelectList Teachers { get; set; }

        public int TeacherFilterId { get; set; }

        public List<ExamAppointment> ExamAppointments { get; set; }

        public void GetExams(IUnitOfWork uow)
        {

            Teachers = new SelectList(uow.TeacherRepository.Get(orderBy: x => x.OrderBy(y => (y.DisplayName))).ToList().Select(d => new SelectListItem
            {

                Text = d.DisplayName,
                Value = d.Id.ToString(),
            }), nameof(SelectListItem.Value), nameof(SelectListItem.Text));

        }

        public void filterByTeacher(IUnitOfWork uow)
        {
            Teachers = new SelectList(uow.TeacherRepository.Get(orderBy: x => x.OrderBy(y => (y.DisplayName))).ToList().Select(d => new SelectListItem
            {
                Text = d.DisplayName,
                Value = d.Id.ToString(),
            }), nameof(SelectListItem.Value), nameof(SelectListItem.Text));

            Exams = uow.ExamRepository.Get(includeProperties: $"{nameof(Exam.Student)},{nameof(Exam.Subject)},{nameof(Exam.Teacher)},{nameof(Exam.SemesterEnd)},{nameof(Exam.Appointment)}",
              filter: f => f.Appointment.Archived == false && f.Teacher.Id == TeacherFilterId,
              orderBy: o => o.OrderBy((u) => u.Student.Sn)
              .ThenBy(e => e.SubjectId)
              .ThenBy(e => e.DepartmentId)).ToList();

            ExamAppointments = new List<ExamAppointment>(Exams.Select(exam => exam.Appointment).Distinct());
        }
    }
}
