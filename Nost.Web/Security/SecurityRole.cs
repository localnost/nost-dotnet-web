﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nost.Web.Security
{
    public enum SecurityRole
    {
        ADMIN,
        TEACHER,
        STUDENT
    }
}
