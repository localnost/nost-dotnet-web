﻿using Nost.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nost.Core.Contracts
{
    public interface IUnitOfWork : IDisposable
    {
        //TODO: add some cool interface variables for repositories
        IGenericRepository<Exam> ExamRepository { get; }

        IGenericRepository<Teacher> TeacherRepository { get; }

        IGenericRepository<Subject> SubjectRepository { get; }

        IGenericRepository<Department> DepartmentRepository { get; }

        IGenericRepository<Student> StudentRepository { get; }

        IGenericRepository<SchoolClass> SchoolClassRepository { get; }

        IGenericRepository<SemesterEnd> SemesterEndRepository { get; }

        IGenericRepository<ExamAppointment> ExamAppointmentRepository{ get; }


        void Save();

        void DeleteDatabase();
    }
}
