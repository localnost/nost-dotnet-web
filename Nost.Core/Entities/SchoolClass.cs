﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Nost.Core.Entities
{
    public class SchoolClass : EntityObject
    {
        [DisplayName("Klasse")]
        public string Name { get; set; }
    }
}
