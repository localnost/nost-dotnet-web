﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Nost.Core.Entities
{
    public class Exam : EntityObject
    {
        [ForeignKey(nameof(Student)), Range(1, Int32.MaxValue)]
        public int StudentId { get; set; }

        public Student Student { get; set; }

        [ForeignKey(nameof(Teacher)), Range(1,Int32.MaxValue,ErrorMessage ="Wählen Sie einen Lehrer aus")]
        public int TeacherId { get; set; }

        public Teacher Teacher { get; set; }

        [ForeignKey(nameof(Appointment))]
        [Required(ErrorMessage = "Wählen Sie einen Prüfungstermin aus")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Wählen Sie einen Prüfungstermin aus")]
        public int? AppointmentId { get; set; }

        [DisplayName("Prüfungstermin"), DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public ExamAppointment Appointment { get; set; }

        [ForeignKey(nameof(Department)),Required(ErrorMessage ="Sie haben keine Abteilung ausgewählt")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Sie haben keine Abteilung ausgewählt")]
        public int DepartmentId { get; set; }

        public Department Department { get; set; }

        [ForeignKey(nameof(Subject))]
        [Required(ErrorMessage ="Sie haben keinen Gegenstand ausgewählt")]
        [Range(1, Int32.MaxValue,ErrorMessage = "Sie haben keinen Gegenstand ausgewählt")]
        public int? SubjectId { get; set; }

        public Subject Subject { get; set; }

        [DisplayName("Semester")]
        [Range(1,10,ErrorMessage ="Wählen Sie ein Semester aus")]
        public int FailedTerm { get; set; }

        [DisplayName("Klasse (negativ beurteilt)"),StringLength(7,MinimumLength =5, ErrorMessage ="Stellen Sie die negative beurteilte Klasse richtig")]
        [Required(ErrorMessage ="Geben sie die negativ beurteilte Klasse an")]
        [RegularExpression("(1|2|3|4|5|6|7|8|9)([A-Z]){3,}",ErrorMessage = "Das Format ist z.B. 1AHIF")]
        public string FailedClass { get; set; }

        [DisplayName("Antrittsart")]
        [Range(0,2)]
        public int Repetition { get; set; }

        [ForeignKey(nameof(SemesterEnd))]
        [Required(ErrorMessage = "Sie haben keinen Ausstellungsdatum ihres Zeugnisses ausgewählt")]
        [Range(1, Int32.MaxValue, ErrorMessage="Sie haben keinen Ausstellungsdatum ihres Zeugnisses ausgewählt")]
        public int? SemesterEndId { get; set; }

        [DisplayName("Ausstellungsdatum des Zeugnisses")]
        public SemesterEnd SemesterEnd { get; set; }

        [DisplayName("Prüfung am PC")]
        public bool IsPcExam { get; set; }

        [DisplayName("Benötigte Software")]
        public string Software { get; set; }

    }
}
