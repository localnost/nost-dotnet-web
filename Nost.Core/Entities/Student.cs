﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Nost.Core.Entities
{
    public class Student: EntityObject
    {
        [DisplayName("Vorname")]
        public string GivenName { get; set; }

        [DisplayName("Nachname")]
        public string Sn { get; set; }

        [DisplayName("Aktuelle Klasse")]
        public string SchoolClass { get; set; }

        [DisplayName("Abteilung")]
        public string Department { get; set; }

        [DisplayName("Nutzername")]
        public string UserName { get; set; }

        [DisplayName("Name")]
        public string FullName
        {
            get
            {
                return $"{Sn} {GivenName}";
            }
        }
    }
}
