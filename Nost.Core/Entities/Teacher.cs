﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Nost.Core.Entities
{
    public class Teacher : EntityObject
    {
        [DisplayName("Kürzel")]
        public string Cn { get; set; }

        [DisplayName("Lehrer")]
        public string DisplayName { get; set; }

        [DisplayName("Archiviert")]
        public bool Archived { get; set; }

        public override bool Equals(object obj)
        {
            var teacher = obj as Teacher;
            return teacher != null &&
                   Cn == teacher.Cn &&
                   DisplayName == teacher.DisplayName &&
                   Archived == teacher.Archived;
        }
    }
}
