﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Nost.Core.Entities
{
    public class ExamAppointment : EntityObject
    {
        [DisplayName("Prüfungstermin"), DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime ExamAppointmentDate { get; set; }


        [DisplayName("Frist"), DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime ExpireDate { get; set; }

        public bool Archived { get; set; }
    }
}
