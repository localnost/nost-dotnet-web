﻿using Nost.Core.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Nost.Core.Entities
{
    public class Subject : EntityObject
    {
        [DisplayName("Gegenstand")]
        [Required(ErrorMessage ="Tragen Sie einen Gegenstandsnamen ein.")]
        public string SubjectName { get; set; }

        [DisplayName("Archiviert")]
        public bool Archived { get; set; }

        [ForeignKey(nameof(Department))]
        [Range(1,Int32.MaxValue,ErrorMessage = "Sie haben keine Abteilung ausgewählt")]
        public int DepartmentId { get; set; }

        [DisplayName("Abteilung")]
        public Department Department { get; set; }

        public override string ToString()
        {
            if (Department != null)
            {
                return $"{SubjectName} ({Department.DepartmentName})";
            }
            else
            {
                return $"{SubjectName}";
            }
        }
    }
}
