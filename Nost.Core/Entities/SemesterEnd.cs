﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Nost.Core.Entities
{
    public class SemesterEnd: EntityObject
    {
        [DisplayName("Semesterende/Zeugnistag"), DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime SemesterEndDate { get; set; }

    }
}
