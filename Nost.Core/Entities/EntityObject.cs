﻿using Nost.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nost.Core.Entities
{
    public class EntityObject : IEntityObject
    {
        public int Id { get; set; }
        public byte[] Timestamp { get; set; }


    }
}
