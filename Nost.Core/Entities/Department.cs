﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;


namespace Nost.Core.Entities
{
    public class Department : EntityObject
    {
        [DisplayName("LDAP-Kürzel")]
        [Required(ErrorMessage ="Geben Sie das LDAP-Kürzel an")]
        public string DepartmentShortcut { get; set; }

        [DisplayName("Abteilung")]
        [Required(ErrorMessage ="Geben Sie einen Namen für diese Abteilung an")]
        public string DepartmentName { get; set; }
    }
}
